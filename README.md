# JFinalCMS 

#### 介绍
JFinalCMS，极速开发，动态添加字段，自定义标签，动态创建数据库表并crud数据，数据库备份、还原，动态添加站点(多站点功能)，一键生成模板代码，让您轻松打造自己的独立网站，同时也方便二次开发，让您快速搭建个性化独立网站，为您节约更多时间，去陪恋人、家人和朋友。

#### 软件架构
1. MVC:JFinal 3.8
2. 页面:enjoy
3. 缓存:ehcache
4. 数据库:Mysql


#### 后台功能

1. 模型管理
2. 栏目管理
3. 内容管理(动态生成)
4. 表单管理(动态生成)
5. 自定义表管理(动态生成)
6. 营销管理(广告、广告位、友情链接)
7. 系统管理(管理员、站点管理、系统配置、模板、清除缓存、数据库)

#### 模板标签

1. 广告位标签#ad_position
2. 子栏目列表标签#category_children_list
3. 单个栏目标签#category
4. 父栏目列表标签#category_parent_list
5. 根栏目列表标签#category_root_list
6. 单个内容标签#content
7. 内容列表标签#content_list
8. 友情链接列表标签#friend_link_list
9. 分页标签#pagination
10. SQL标签#sql_list
11. 内容分页标签#content_page

#### 交流

1. 联系QQ：644080923 交流群:575535321
2. 官网地址:[http://www.jrecms.com](http://www.jrecms.com)
3. 体验地址: 前台:http://cms.jrecms.com 后台:http://cms.jrecms.com/admin/login 账号:admin 123456

#### 帮助文档


#### 案例

![案例](https://images.gitee.com/uploads/images/2019/0522/142523_9cedc51f_623319.png "屏幕截图.png")

#### 后台功能截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0102/192142_2da79cc9_623319.png "屏幕截图.png")

####  捐献

1. 捐献二维码<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2019/1214/135512_cb408dd6_623319.png "shoukuan.png")
2. 联系：  微信：heyewei123   QQ:644080923
3. 默认赠送4套模板
（1）PC中文<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0102/192441_bf5da843_623319.png "屏幕截图.png")
（2）PC英文<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0102/193554_f2f19495_623319.png "屏幕截图.png")
（3）模板1<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0102/193645_0895b3a7_623319.png "屏幕截图.png")
（4）小程序<br/>
